from Components import Common

class Link(Common.Common):
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def click_link(self, label):
        xpath = "//a[text()='"+label+"']"
        self.wait_for_element_to_appear(self.driver, xpath)
        self.click(self.driver, xpath)