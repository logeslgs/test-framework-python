from Components import Common

class Button(Common.Common):
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def click_button(self, label):
        xpath = "//button[text()='"+label+"']"
        self.wait_for_element_to_appear(self.driver, xpath)
        self.click(self.driver, xpath)