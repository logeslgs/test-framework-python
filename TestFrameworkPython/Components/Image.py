from Components import Common

class Image(Common.Common):
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def click_image(self, label):
        xpath = "//img[@alt='"+label+"']"
        self.wait_for_element_to_appear(self.driver, xpath)
        self.click(self.driver, xpath)

    def get_image_count(self, label):
        count = 0
        xpath = "//img[@alt='" + label + "']"
        self.wait_for_element_to_appear(self.driver, xpath)
        count = len(self.driver.find_elements_by_xpath(xpath))
        return count