from Components import Common

class TextField(Common.Common):
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def set_text_field_value(self, label, value):
        xpath = "//input[@placeholder='"+label+"']";
        self.wait_for_element_to_appear(self.driver, xpath)
        self.set_value(self.driver, xpath, value)