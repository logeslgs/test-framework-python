from Components import Common

class Label(Common.Common):
    driver = None

    def __init__(self, driver):
        self.driver = driver

    def get_label_count(self, label):
        count = 0
        xpath = "//div[@class='"+label+"']|//div[contains(@id,'"+label+"')]"
        self.wait_for_element_to_appear(self.driver, xpath)
        count = len(self.driver.find_elements_by_xpath(xpath))
        return count

    def verify_label_displayed(self, label):
        xpath = "//div[@class='" + label + "']|//div[contains(@id,'" + label + "')]"
        self.wait_for_element_to_appear(self.driver, xpath)
        return self.is_element_displayed(self.driver, xpath)