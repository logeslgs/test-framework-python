from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

class Common:
    timeout = 60

    def wait_for_element_to_appear(self, driver, locator):
        element = WebDriverWait(driver, self.timeout).until(EC.visibility_of_element_located((By.XPATH, locator)))


    def set_value(self, driver, locator, value):
        element = driver.find_element_by_xpath(locator)
        element.send_keys(value)

    def click(self, driver, locator):
        element = WebDriverWait(driver, self.timeout).until(EC.element_to_be_clickable((By.XPATH, locator)))
        element.click()

    def is_element_displayed(self, driver, locator):
        element = driver.find_element_by_xpath(locator)
        if(element.is_displayed()):
            elementfound = True
        else:
            elementfound = False
        return elementfound

    def wait_for_element_to_disappear(self, driver, locator):
        element = WebDriverWait(driver, self.timeout).until(EC.invisibility_of_element_located((By.XPATH, locator)))
