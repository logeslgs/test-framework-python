from Components import TextField
from Components import Button
from Components import Link
from Panels import CommonUtils

class Dashboard(CommonUtils.CommonUtils):
    driver = None

    def __init__(self):
        self.textfield = TextField.TextField(CommonUtils.CommonUtils.driver)
        self.button = Button.Button(CommonUtils.CommonUtils.driver)
        self.link = Link.Link(CommonUtils.CommonUtils.driver)

    def set_login_username(self,value):
        self.textfield.set_text_field_value("Work E-mail", value)

    def set_login_password(self,value):
        self.textfield.set_text_field_value("Password", value)

    def click_login(self):
        self.button.click_button("Log In")
        self.button.wait_for_element_to_disappear(CommonUtils.CommonUtils.driver, "//div[contains(@class,'loader') and contains(@class,'overlay')]")

    def click_experiences(self):
        self.link.click_link("Experiences")

    def edit_liveexperiece(self):
        xpath = "//span[text()='Status: ']/following-sibling::div[text()='Live']/ancestor::div[@role='presentation']//div[text()='Edit']"
        self.button.wait_for_element_to_appear(CommonUtils.CommonUtils.driver, xpath)
        self.button.click(CommonUtils.CommonUtils.driver, xpath)