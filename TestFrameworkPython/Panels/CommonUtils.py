from selenium import webdriver
import os
import xlrd


class CommonUtils:
    sheet = None
    driver = None
    currentpath = os.path.dirname(os.path.abspath("."))

    @staticmethod
    def launchbrowser(self, browsertype):
        if(browsertype.lower() == "ie"):
            CommonUtils.driver = webdriver.Ie(self.currentpath+"/IEDriverServer.exe")
        elif(browsertype.lower() == "firefox"):
            CommonUtils.driver = webdriver.Firefox(self.currentpath+"/geckodriver.exe")
        elif(browsertype.lower() == "chrome"):
            CommonUtils.driver = webdriver.Chrome(self.currentpath+"/chromedriver.exe")
        else:
            raise TypeError("Unknown Browser Type.")
        return CommonUtils.driver

    def navigatetourl(self, url):
        if isinstance(url, str):
            CommonUtils.driver.get(url)
        else:
            raise TypeError("URL must be a string.")

    def getsheet(self, workbookname, sheetname):
        wb = xlrd.open_workbook(workbookname)
        self.sheet = wb.sheet_by_name(sheetname)
        return self.sheet

    def readData(self, columnname, rownum):
        self.sheet.cell_value(0, 0)
        columnindex = 0
        for i in range(self.sheet.ncols):
            if(self.sheet.cell_value(0, i) == columnname):
                columnindex = i
                break

        return self.sheet.cell_value(rownum, columnindex)