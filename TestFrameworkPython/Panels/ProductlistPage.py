from Components import Label
from Components import Image
from Panels import CommonUtils

class ProductlistPage(CommonUtils.CommonUtils):
    driver = None

    def __init__(self):
        self.label = Label.Label(CommonUtils.CommonUtils.driver)
        self.image = Image.Image(CommonUtils.CommonUtils.driver)

    def get_product_count(self):
        count = self.label.get_label_count("ProductCardImageWrapper")
        return count

    def get_experience_count(self):
        count = self.image.get_image_count("Style it with")
        return count
    def verify_experience_displayed(self):
        return self.label.verify_label_displayed("vue-carousel-slide-item-wrapper")

    def click_experience(self):
        self.image.click_image("Style it with")