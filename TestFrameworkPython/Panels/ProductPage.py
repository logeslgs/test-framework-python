from Components import Label
from Panels import CommonUtils

class ProductPage(CommonUtils.CommonUtils):
    driver = None

    def __init__(self):
        self.label = Label.Label(CommonUtils.CommonUtils.driver)


    def verify_experience_displayed(self):
        return self.label.verify_label_displayed("vue-item-wrapper vue-slider-item-wrapper")

    def get_recommendation_count(self):
        count = self.label.get_label_count("vue-item-wrapper vue-slider-item-wrapper")
        return count
