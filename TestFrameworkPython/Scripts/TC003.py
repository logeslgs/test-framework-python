import unittest

from Panels import CommonUtils
from Panels import Dashboard


class TC003(unittest.TestCase):
    commonutils = CommonUtils.CommonUtils()
    def setUp(self):
        self.sheet = self.commonutils.getsheet("../Data/Data.xlsx", self.__class__.__name__)


    def test_dashboard(self):
        rownum = 1
        while True:
            self.driver = self.commonutils.launchbrowser(self.commonutils, self.commonutils.readData("BrowserType", rownum))
            self.commonutils.navigatetourl(self.commonutils.readData("URL", rownum))
            dashboard = Dashboard.Dashboard()
            dashboard.set_login_username(self.commonutils.readData("UserName", rownum))
            dashboard.set_login_password(self.commonutils.readData("Password", rownum))
            dashboard.click_login()
            dashboard.click_experiences()
            dashboard.edit_liveexperiece()
            self.driver.close()
            if(rownum >= self.sheet.nrows - 1):
                break
            rownum = rownum + 1

    def tearDown(self):
        self.driver.quit()
