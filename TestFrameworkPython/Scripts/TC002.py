import unittest

from Panels import CommonUtils
from Panels import ProductPage


class TC002(unittest.TestCase):
    commonutils = CommonUtils.CommonUtils()
    def setUp(self):
        self.sheet = self.commonutils.getsheet("../Data/Data.xlsx", self.__class__.__name__)


    def test_product_page(self):
        rownum = 1
        while True:
            self.driver = self.commonutils.launchbrowser(self.commonutils, self.commonutils.readData("BrowserType", rownum))
            self.commonutils.navigatetourl(self.commonutils.readData("URL", rownum))
            productpage = ProductPage.ProductPage()
            assert productpage.verify_experience_displayed()
            rec_count = productpage.get_recommendation_count()
            assert rec_count > 0
            self.driver.close()
            if(rownum >= self.sheet.nrows - 1):
                break
            rownum = rownum + 1

    def tearDown(self):
        self.driver.quit()
