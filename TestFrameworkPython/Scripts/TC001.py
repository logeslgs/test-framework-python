import unittest

from Panels import CommonUtils
from Panels import ProductlistPage


class TC001(unittest.TestCase):
    commonutils = CommonUtils.CommonUtils()
    def setUp(self):
        self.sheet = self.commonutils.getsheet("../Data/Data.xlsx", self.__class__.__name__)


    def test_product_list_page(self):
        rownum = 1
        while True:
            self.driver = self.commonutils.launchbrowser(self.commonutils, self.commonutils.readData("BrowserType", rownum))
            self.commonutils.navigatetourl(self.commonutils.readData("URL", rownum))
            productlistpage = ProductlistPage.ProductlistPage()
            productcount = productlistpage.get_product_count()
            experiencecount = productlistpage.get_experience_count()
            assert productcount == experiencecount
            productlistpage.click_experience()
            assert productlistpage.verify_experience_displayed()
            self.driver.close()
            if(rownum >= self.sheet.nrows - 1):
                break
            rownum = rownum + 1

    def tearDown(self):
        self.driver.quit()
